#!/usr/bin/env python3

# Part 1 - task from slide 29
class Mammal: pass


class Lion(Mammal): pass


class Cream: pass


class FaceCream(Cream): pass


class Bakery: pass


class Donuts(Bakery): pass

# Part 2 - task from slide 32


class Chess: pass


class Figure(Chess): pass


class Queen(Figure): pass


a = Chess()
b = Figure()
c = Queen()

print("="*60)
print(isinstance(a, object))
print(isinstance(b, object))
print(isinstance(c, object))

print("="*60)
print(isinstance(a, Chess))
print(isinstance(b, Chess))
print(isinstance(c, Chess))

print("="*60)
print(isinstance(a, Figure))
print(isinstance(b, Figure))
print(isinstance(c, Figure))

print("="*60)
print(isinstance(a, Queen))
print(isinstance(b, Queen))
print(isinstance(c, Queen))

# Part 3 - tasks from slide 41


def print_my_name() -> str:
    return "Dawid"


def sum_of_3_numbers(a: int, b: int, c: int) -> int:
    return a+b+c


def hello_world() -> str:
    return "Cześć świecie"


def subtraction_of_2_numbers(a: int, b: int) -> int:
    return a-b


class Knight:
    def __init__(self):
        print("Created a new knight.")


def new_knight() -> Knight:
    return Knight()


print("="*60)
print("Your name is " + print_my_name())

print("="*60)
print("Result: " + str(sum_of_3_numbers(3,2,3)))

print("="*60)
print(hello_world())

print("="*60)
print("Result: " + str(subtraction_of_2_numbers(5,-6)))
print("Result: " + str(subtraction_of_2_numbers(-5,6)))

print("="*60)
new_knight()
